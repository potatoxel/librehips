import { ClientHandler } from "../ClientHandler";
import { ServerGameObject } from "../ServerGameObject";

export class ServerScreen extends ServerGameObject {
    protected completeGame: () => void = () => null;

    constructor() {
        super();
    }

    getCompletionData(): any {
        return null;
    }

    waitUntilCompleted(): Promise<any> {
        return new Promise<any>((executor, reject) => {
            this.completeGame = () => {
                executor(this.getCompletionData());
            };
        });
    }

    getClientStatus(client: ClientHandler): string {
        return "";
    }
}
