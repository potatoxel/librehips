//#region PREAMBLE
/*
    This is a multiplayer game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { ClientHandler } from "./ClientHandler";
import { NetworkWorld } from "./NetworkWorld";
import { Room } from "./Room";

export class GameBase {
    protected gameInProgress: boolean = false;
    protected room: Room;
    public clients = new Array<ClientHandler>();
    public clientsId = new Map<ClientHandler, number>();

    private timers = new Set<NodeJS.Timeout>();

    public world: NetworkWorld;
    
    public screen: string;
    public name: string;

    constructor(room: Room) {
        this.room = room;
    }

    addClient(client: ClientHandler) {
        this.clientsId.set(client, this.clients.length);
        this.clients.push(client);
    }

    removeClient(client: ClientHandler) {
        //TODO: There may be an issue here.
        //TODO: allow rejoining, or maybe that should be done in room.
        this.clients.splice(this.clients.indexOf(client),1);
    }

    isGameDone() {
        return false;
    }

    getClientStatus(client: ClientHandler) {
        return "";
    }

    async start() {
        
    }

    addAllClientsFrom(game: GameBase) {
        game.clients.forEach(client => {
            this.addClient(client);
        })
    }

    dispose() {
        this.timers.forEach((value) => {
            clearInterval(value);
        });
    }

    setInterval(func: () => void, time: number) {
        this.timers.add(setInterval(() => {
            func()
        }, time));
    }
}