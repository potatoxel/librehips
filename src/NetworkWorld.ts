//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { World } from '../client/shared/World';
import { ClientHandler } from './ClientHandler';
import { RemoveGameObjectData } from '../client/shared/Packets';
import { Server } from './Server';
import { ServerGameObject } from './ServerGameObject';
import * as fs from 'fs';
import * as path from 'path';
import { Vector3 } from '../client/shared/Vector3';
import { ServerPrefabInstantiator } from './ServerPrefabInstantiator';
import { ServerSerializedGameObject } from './ServerSerializedGameObject';
import { GameObject } from '../client/shared/GameObject';
import { Room } from './Room';
import { GameBase } from './GameBase';

type WorldSaveFormat = {
    spawnPoint: Vector3,
    gameObjects: Array<ServerSerializedGameObject>
}

export class NetworkWorld extends World {
    private freeId = 0;

    public instantiator = new ServerPrefabInstantiator();
    spawnPoint: Vector3 = new Vector3(0, 0, 0);

    public room: Room;
    public game: GameBase;

    constructor(room: Room, game: GameBase) {
        super();
        this.room = room;
        this.game = game;
    }

    addChild(gameObject: ServerGameObject) {
        gameObject.world = this;
        gameObject.id = this.getFreeId();
        super.addChild(gameObject, false);
        this.room.broadcast('spawnGameObject', gameObject.getPublicData());
        gameObject.ready();
    }

    queueRemoveChild(gameObject: ServerGameObject) {
        super.queueRemoveChild(gameObject);
        this.room.broadcast('removeGameObject', {
            id: gameObject.id
        } as RemoveGameObjectData);
    }

    removeChild(gameObject: ServerGameObject) {
        super.removeChild(gameObject);
        this.room.broadcast('removeGameObject', {
            id: gameObject.id
        } as RemoveGameObjectData);
    }

    sendWorldTo(client: ClientHandler) {
        this.children.forEach((gameObject: ServerGameObject, key, map) => {
            client.emit('spawnGameObject', gameObject.getPublicData());
        });
    }

    serialize() {
        let data = new Array<ServerSerializedGameObject>();
        
        this.children.forEach((gameObject: ServerGameObject, key, map) => {
            if(gameObject.shouldBeSerialized)
                data.push(gameObject.serialize());
        });

        let res: WorldSaveFormat = {
            spawnPoint: this.spawnPoint,
            gameObjects: data
        };
        return res;
    }

    save() {
        console.log("Saved to: " + path.join(__dirname, 'test.json'));
        fs.writeFile(path.join(__dirname, '../test.json'), JSON.stringify(this.serialize()), ()=>{});
    }

    load() {
        try {
            const data: WorldSaveFormat = JSON.parse(fs.readFileSync(path.join(__dirname, '../test.json'), 'utf8'));
            
            data.gameObjects.forEach((objData, index, array) => {
                var gameObject = this.instantiator.instantiateServerObject(objData);
                this.addChild(gameObject);
            });

            this.spawnPoint = new Vector3(data.spawnPoint.x, data.spawnPoint.y, data.spawnPoint.z);
            
            console.log("loaded world!");
        } catch (err) {
            console.error(err);
        }
    }

    private getFreeId() {
        return this.freeId++;
    }

    findEntitiesWithinRadius(position: Vector3, radius: number) {
        let res: Array<GameObject> = [];
        this.children.forEach((gameObject, key, map) => {
            if(gameObject instanceof GameObject) {
                if(gameObject.position.sub(position).length() <= radius) {
                    res.push(gameObject);
                }
            }
        });
        return res;
    }

    findEntitiesPreciseCollidingWithPoint(position: Vector3) {
        let res: Array<ServerGameObject> = [];
        this.children.forEach((gameObject, key, map) => {
            if(gameObject.preciseCollidesWithPoint(position)) 
                res.push(gameObject as ServerGameObject);
        });
        return res;
    }

    findEntitiesCollidingWithPoint(position: Vector3) {
        let res: Array<ServerGameObject> = [];
        this.children.forEach((gameObject, key, map) => {
            if(gameObject.collidesWithPoint(position)) 
                res.push(gameObject as ServerGameObject);
        });
        return res;
    }

    findEntitiesCollidingWith(gameObject: ServerGameObject) {
        let res: Array<ServerGameObject> = [];
        this.children.forEach((other, key, map) => {
            if(other != gameObject && gameObject.collidesWith(other)) 
                res.push(other as ServerGameObject);
        });
        return res;
    }

    raycast (origin: Vector3, direction: Vector3, max_steps: number, except: ServerGameObject): Array<{gameObject: ServerGameObject, position: Vector3}> {
        let current_position = origin;
        for(let i = 0; i < max_steps; i++) {
            let collisions = this.findEntitiesPreciseCollidingWithPoint(current_position);
            let res: Array<{gameObject: ServerGameObject, position: Vector3 }> = [];
            for(let j = 0; j < collisions.length; j++) {
                if(except != collisions[j]) {
                    res.push({
                        gameObject: collisions[j],
                        position: current_position
                    });
                }
            }
            if(res.length > 0) {
                return res;
            }
            current_position = current_position.add(direction);
        }
        return [];
    }
}
