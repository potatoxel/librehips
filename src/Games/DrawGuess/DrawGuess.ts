import { Vector3 } from "../../../client/shared/Vector3";
import { ClientHandler } from "../../ClientHandler";
import { ServerScreen } from "../../engine/ServerScreen";
import { GameBase } from "../../GameBase";
import { NetworkWorld } from "../../NetworkWorld";
import { ServerGameObject } from "../../ServerGameObject";
import { Drawing, PacketOptions, PacketSendImage, PacketSendPrompt, PacketSetImageToView, PacketShowImage, PacketStoreImage } from "../../../client/Games/DrawGuess/shared/DrawGuessTypes"

class GamePlayer {
    client: ClientHandler;
    done: boolean = false;

    constructor(client: ClientHandler){ 
        this.client = client;
    }

    getStatus() {
        return "";
    }

}

type ImageAndPrompt = { 
    image: string;
    original_prompt: string;    
}

type GuessAndPrompt = {
    guess: string;
    original_prompt: string;  
}

class PromptScreen extends ServerScreen {
    prefab = "promptScreen";
    prompts = new Map<ClientHandler, GuessAndPrompt>();

    ready() {
        super.ready();
        let game = this.world.game as DrawGuessGame;

        this.messageHandler.on("prompt", (sender, data: PacketSendPrompt) => {
            this.prompts.set(sender, {
                original_prompt: data.prompt,
                guess: data.prompt
            });
            game.gamestate.players.get(sender).done = true;
            this.world.room.updatePlayerList();  
            if (game.gamestate.allDone()) {
                this.completeGame();
            }
        });

    }


    getClientStatus(client: ClientHandler): string {
        let game = this.world.game as DrawGuessGame;
        return "Done: " + game.gamestate.players.get(client).done;
    }

    getCompletionData() {
        let game = this.world.game as DrawGuessGame;
        return game.clients.map(v => this.prompts.get(v));
    }
}


class DrawingScreen extends ServerScreen {
    prefab = "drawingScreen";
    drawing = new Map<ClientHandler, ImageAndPrompt>();

    constructor(private prompts: GuessAndPrompt[]) {
        super()
    }

    ready() {
        super.ready();
        let game = this.world.game as DrawGuessGame;
        game.clients.forEach((client, id) => {
            this.emitTo(client, "prompt", {
                prompt: this.prompts[id].guess
            })
        });

        this.messageHandler.on("submit", (sender, data: PacketSendImage) => {
            this.drawing.set(sender, {
                original_prompt: this.prompts[game.clientsId.get(sender)].original_prompt,
                image: data.image
            });
            game.gamestate.players.get(sender).done = true;
            this.world.room.updatePlayerList();  
            if (game.gamestate.allDone()) {
                this.completeGame();
            }
        });
    }

    getClientStatus(client: ClientHandler): string {
        let game = this.world.game as DrawGuessGame;
        return "Done: " + game.gamestate.players.get(client).done;
    }

    getCompletionData() {
        let game = this.world.game as DrawGuessGame;
        return game.clients.map(v => this.drawing.get(v));
    }
}

class GuessingScreen extends ServerScreen {
    prefab = "guessingScreen";
    guessing = new Map<ClientHandler, GuessAndPrompt>();

    constructor(private images: ImageAndPrompt[]) {
        super()
    }

    ready() {
        super.ready();
        let game = this.world.game as DrawGuessGame;

        game.clients.forEach((client, id) => {
            this.emitTo(client, "image", {
                image: this.images[id].image
            } as PacketSendImage)
        });

        this.messageHandler.on("submit", (sender, data: PacketSendPrompt) => {
            this.guessing.set(sender, {
                original_prompt: this.images[game.clientsId.get(sender)].original_prompt,
                guess: data.prompt
            });
            game.gamestate.players.get(sender).done = true;
            this.world.room.updatePlayerList();  
            if (game.gamestate.allDone()) {
                this.completeGame();
            }
        });
    }


    getClientStatus(client: ClientHandler): string {
        let game = this.world.game as DrawGuessGame;
        return "Done: " + game.gamestate.players.get(client).done;
    }

    getCompletionData() {
        let game = this.world.game as DrawGuessGame;
        return game.clients.map(v => this.guessing.get(v));
    }
}

class ViewingScreen extends ServerScreen {
    prefab = "viewingScreen";

    ready() {
        super.ready();
        let game = this.world.game as DrawGuessGame;
        this.world.room.updatePlayerList();  
        
        let res: PacketOptions = {
            options: []
        };
        
        game.gamestate.gameResult.forEach((value, key) => {
            let arr: string[] = [];
            for(let i = 0; i < value.length; i++) {
                arr.push(i.toString());
            }

            res.options.push({ 
                name: key,
                suboptions: arr
            })
        });

        this.emit("options", res);

        this.messageHandler.on("setImageToView", (sender, data: PacketSetImageToView) => {
            this.emit("showImage", {
                prompt: data.prompt,
                id: data.view
            } as PacketShowImage)
        })

        this.messageHandler.on("backToLobby", (sender, data) => {
            this.world.room.toLobby();
        })
    }

    getClientStatus(client: ClientHandler): string {
        let game = this.world.game as DrawGuessGame;
        return "Looking :o";
    }

    getCompletionData() {
        return ""
    }
}

class GameState extends ServerGameObject {
    public players = new Map<ClientHandler, GamePlayer>();
    prefab = "gamestate";
    currentScreen: ServerScreen;
    gameResult: Map<string, Drawing[]>;

    async ready() {
        this.gameResult = new Map<string, Array<Drawing>>();
        this.world.game.clients.forEach((client, i) => {
            this.players.set(client, new GamePlayer(client));
        });

        let prompts: GuessAndPrompt[] = await this.runScreen(new PromptScreen());   
        prompts.forEach((prompt) => {
            this.gameResult.set(prompt.original_prompt, []);
        });

        let last_guesses = prompts; // A B C D
        for(let i = 0; i < 3; i++) {
            let drawings: ImageAndPrompt[] = await this.runScreen(new DrawingScreen(rotate(last_guesses, 1)));  // D A B C
            let guesses: GuessAndPrompt[] = await this.runScreen(new GuessingScreen(rotate(drawings, 1))); // C D A B
    
           for(let id = 0; id < prompts.length; id++){
                let prompt = last_guesses[id]; // >A B C D
                let drawing = drawings[mod(id+1, drawings.length)] // D >A B C
                let guess = guesses[mod(id+2, guesses.length)] // C D >A B
                const info = {
                    prompt: prompt.guess,
                    drawing: drawing.image,
                    guess: guess.guess,
                };
                this.gameResult.get(prompt.original_prompt).push(info);
                this.emit("storeImage", {
                    original_prompt: guess.original_prompt,
                    info: info
                } as PacketStoreImage);
            };

            last_guesses = guesses;
        }
        await this.runScreen(new ViewingScreen())

    }
    
    async runScreen(screen: ServerScreen) {
        if(this.currentScreen) {
            this.world.queueRemoveChild(this.currentScreen);
        }
        this.setNotDone();
        this.currentScreen = screen;  
        this.world.addChild(screen);  
        return await this.currentScreen.waitUntilCompleted();
    }

    allDone() {
        return Array.from(this.players.values()).every((value) => value.done);
    }

    setNotDone() {
        this.players.forEach(v => { v.done = false; });
    }
}

export class DrawGuessGame extends GameBase {
    name = "drawGuessGame";
    world: NetworkWorld;
    gamestate: GameState;

    async start() {
        this.room.changeScreen("screenGame");
        this.world = new NetworkWorld(this.room, this);
        this.gamestate = new GameState();
        this.world.addChild(this.gamestate);
    }

    getClientStatus(client: ClientHandler) {
        return this.gamestate.currentScreen.getClientStatus(client);
    }
}



function rotate<T>(array: T[], amount: number): T[] {
    let res = new Array<T>();
    res.length = array.length;
    for(let i = 0; i < array.length; i++) {
        res[mod(i+amount, array.length)] = array[i];
    }
    return res;
}

function mod(x: number, y: number): number {
    return (x % y + y) % y;
}