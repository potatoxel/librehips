import { GameObject } from "../../../client/shared/GameObject";
import { Vector3 } from "../../../client/shared/Vector3";
import { ClientHandler } from "../../ClientHandler";
import { GameBase } from "../../GameBase";
import { NetworkWorld } from "../../NetworkWorld";
import { ServerGameObject } from "../../ServerGameObject";

class GamePlayer {
    points: number = 0;
    client: ClientHandler;

    constructor(client: ClientHandler){ 
        this.client = client;
    }

    getStatus() {
        return "points: " + this.points;
    }
}

class Tilemap extends ServerGameObject {
    prefab = "tilemap";

    ready() {
        this.messageHandler.on("setBlock", (sender, data) => {
            this.emit("setBlock", data, new Set([sender]));
        });
    }
}

class GameState extends ServerGameObject {
    public players = new Map<ClientHandler, GamePlayer>();
    public pointsLeft = 10;
    prefab = "gamestate";

    ready() {
        this.world.game.clients.forEach((client, i) => {
            this.players.set(client, new GamePlayer(client));
        });

        let tilemap = new Tilemap();
        this.world.addChild(tilemap);


        this.world.game.clients.forEach(client => {
            let player = new Player();
            player.client = client;
            player.position = new Vector3(0,0,4);
            this.world.addChild(player);
        });
    }

}

class Player extends ServerGameObject {
    prefab = "player";
    alive = true;
    client: ClientHandler;

    ready() {
        super.ready();
        this.client.emit("receiveLocalPlayerId", {
            id: this.id
        });

        this.messageHandler.on("setPosition", (sender, data) => {
            if(!this.alive) return;
            this.position = new Vector3(data.x, data.y, data.z);
            this.emitPosition();
        });
    }

}

export class SpleefGame extends GameBase {
    name = "spleefGame";
    world: NetworkWorld;
    gamestate: GameState;

    async start() {
        this.room.changeScreen("screenGame");
        this.world = new NetworkWorld(this.room, this);
        this.gamestate = new GameState();
        this.world.addChild(this.gamestate);
    }

    getClientStatus(client: ClientHandler) {
        return this.gamestate.players.get(client).getStatus();
    }
}