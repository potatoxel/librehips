import { Vector3 } from "../../../client/shared/Vector3";
import { ClientHandler } from "../../ClientHandler";
import { GameBase } from "../../GameBase";
import { NetworkWorld } from "../../NetworkWorld";
import { ServerGameObject } from "../../ServerGameObject";

class GamePlayer {
    points: number = 0;
    client: ClientHandler;

    constructor(client: ClientHandler){ 
        this.client = client;
    }

    getStatus() {
        return "points: " + this.points;
    }
}

class Point extends ServerGameObject {
    prefab = "point";

    ready() {
        let game = this.world.game as LiacGame;
        this.messageHandler.on("click", (sender, data) => {
            game.gamestate.collectPointFor(sender);
            this.world.queueRemoveChild(this);
            this.world.room.updatePlayerList();
        });
    }

}

class GameState extends ServerGameObject {
    public players = new Map<ClientHandler, GamePlayer>();
    public pointsLeft = 10;
    prefab = "gamestate";

    ready() {
        this.world.game.clients.forEach((client, i) => {
            this.players.set(client, new GamePlayer(client));
        });

        for(let i = 0; i < this.pointsLeft; i++) {
            let obj = new Point();
            obj.position = new Vector3(Math.random()*550, Math.random()*550, 0);
            this.world.addChild(obj);
        }
    }

    collectPointFor(client: ClientHandler) {
        this.players.get(client).points += 1;
        this.pointsLeft -= 1;
        
        if(this.pointsLeft <= 0) {
            let winnerName = "";
            let winnerAmount = 0;
            this.players.forEach(player => {
                if(player.points >= winnerAmount) {
                    winnerAmount = player.points;
                    winnerName = player.client.name;
                }
            });
            this.world.room.alert("Game completed! Winner: " + winnerName + " with " + winnerAmount + " points.");
            this.world.room.toLobby();
        }
    }

}

export class LiacGame extends GameBase {
    name = "liacGame";
    world: NetworkWorld;
    gamestate: GameState;

    async start() {
        this.room.changeScreen("screenGame");
        this.world = new NetworkWorld(this.room, this);
        this.gamestate = new GameState();
        this.world.addChild(this.gamestate);
    }

    getClientStatus(client: ClientHandler) {
        return this.gamestate.players.get(client).getStatus();
    }
}