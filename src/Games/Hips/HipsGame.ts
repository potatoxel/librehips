import { SettingPositionData } from "../../../client/shared/Packets";
import { Vector3 } from "../../../client/shared/Vector3";
import { ClientHandler } from "../../ClientHandler";
import { GameBase } from "../../GameBase";
import { NetworkWorld } from "../../NetworkWorld";
import { ServerGameObject } from "../../ServerGameObject";
import { WinnerInfoPacket } from "../../../client/Games/Hips/shared/HipsPackets";

class Living extends ServerGameObject {
    alive = true;
    win = false;

    ready() {
        this.messageHandler.on("shoot", (sender, data) => {
            let game = this.world.game as HipsGame;
            if(game.players.get(sender).alive) {
                this.alive = false;
                this.emit("died", null);
            }
        });
    }

    update() {
        super.update();
        if(this.position.x >= 600-32 && !this.win) {
            this.win = true;
            let game = this.world.game as HipsGame;
            game.gamestate.win(this);
        }
    }
}

class Player extends Living {
    prefab = "player";
    alive = true;
    client: ClientHandler;

    ready() {
        super.ready();
        this.client.emit("receiveLocalPlayerId", {
            id: this.id
        });

        this.messageHandler.on("setPosition", (sender, data: SettingPositionData) => {
            if(!this.alive) return;
            this.position = new Vector3(data.x, data.y, data.z);
            this.emitPosition();
        });
    }

}


class NPC extends Living {
    prefab = "npc";
    speed = 0;

    async ready() {
        super.ready();

        this.every(10, ()=> {
            let rand = Math.random();
            if(this.speed == 0) {
                if(rand >= 0.95) {
                    this.speed = this.position.x >= 400 ? 5 : 1;
                }
            } else if(this.speed == 1) {
                if(rand <= 0.15) {
                    this.speed = 0;
                } else {
                    this.speed = this.position.x >= 400 ? 5 : 1;
                }
            }
        });
    }

    update() {
        super.update();
        if(this.alive) {
            this.position.x += this.speed;
            this.emitPosition();
        }
    }
    
}

class Cursor extends ServerGameObject {
    prefab = "cursor";
    player: Player;

    ready() {
        this.messageHandler.on("setPosition", (sender, data: SettingPositionData) => {
            this.position = new Vector3(data.x, data.y, data.z);
            this.emitPosition();
        });

        this.messageHandler.on("shoot", (sender, data) => {
            if(this.player.alive)
                this.emit("shoot", null);
        });
    }
}

class GameState extends ServerGameObject {
    prefab = "gamestate";

    win(living: Living) {
        this.emit("win", {
            id: living.id,
            isPlayer: living instanceof Player,
            name: living instanceof Player ? living.client.name : "NPC"
        } as WinnerInfoPacket);

        this.world.children.forEach((child) => {
            if(child instanceof Living) {
                child.alive = false;
            }
        });

        this.world.room.toLobby();
    }
}

export class HipsGame extends GameBase {
    gamestate = new GameState();
    players = new Map<ClientHandler, Player>();
    name = "hipsGame";
    
    async start() {
        this.world = new NetworkWorld(this.room, this);
        this.world.addChild(this.gamestate);    
        let players = new Array<Living>();

        this.clients.forEach(client => {
            let player = new Player();
            player.client = client;
            players.push(player);
            this.players.set(client, player);

            let cursor = new Cursor();
            cursor.player = player;
            this.world.addChild(cursor);
            cursor.emitTo(client, "setOwner", null);
        });

        for(let i = 0; i < 4; i++) {
            let npc = new NPC();
            players.push(npc);
        }

        for(let i = 0; i < players.length; i++) {
            let temp = players[i];
            let rand = Math.floor(Math.random()*players.length);
            players[i] = players[rand];
            players[rand] = temp;
        }

        let y = 0;
        players.forEach(player => {
            player.position = new Vector3(0, y*48, 0);
            this.world.addChild(player);
            y++;
        });

        this.room.changeScreen("screenGame");

        this.setInterval(() => {
            this.world.update();
        }, 100);
    }
}