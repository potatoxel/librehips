import { Vector3 } from "../../../client/shared/Vector3";
import { ClientHandler } from "../../ClientHandler";
import { GameBase } from "../../GameBase";
import { NetworkWorld } from "../../NetworkWorld";
import { ServerGameObject } from "../../ServerGameObject";
import { PacketPickWords, PacketSendSentence, PacketSendSentences, PacketSendVotes, PacketSendWord, PacketSendWords, PacketSentenceWon, PacketVoteSentence } from "../../../client/Games/Xiatc/shared/XiatcPackets";
import { shuffle_array_in_place } from "../../../client/shared/Utils";
import { ServerScreen } from "../../engine/ServerScreen";

const prompts = [
    ["There once was a ", "-blank", " that went to ", "-blank"],
    ["I am you biggest ", "-blank", " that is why i bought you ", "-blank"],
    ["This guy, ", "-blank", ", told me i should shut up!"],
    ["Breaking news! ", "-blank", " did ", "-blank", " which caused them to die."],
    ["Dont do ", "-blank", " kids its not ", "-blank", " for you!"],
    ["This piece of ", "-blank", " told me i should go to ", "-blank", "land"],
    ["I told you, ", "-blank", " tastes really good."],
    ["I talked to my ", "-blank", " on mastodon. Then ", "-blank", " happened!"],
    ["I like to eat ", "-blank", " everyday!"],
    ["This is very important, you must ", "-blank", "-blank"],
    ["I met ", "-blank", " in the ", "-blank", " matrix room."],
    ["The outside world is filled with ", "-blank"],
    ["-blank", " is a very interesting substance."],
    ["Scientists have discovered that ", "-blank", " should be consumed daily because its very ", "-blank"],
    ["I am concerned about your consumption of ", "-blank", ", you might ", "-blank", " someday."],
    ["Please consume ", "-blank", " daily as ", "-blank", " is ", "-blank", " for you!"],
    ["I will go to ", "-blank", " to purchase as much ", "-blank", " as possible!"],
    ["I heard that ", "-blank", " and ", "-blank", " where talking about ", "-blank", "."],
    ["I feel ", "-blank", " today... Maybe I should take ", "-blank"],
    ["Next time ", "-blank", " is necessary for human survival"],
    ["Not medical advice, but ", "-blank", " is good for you! Not ", "-blank", " advice either."],
    ["I asked my ", "-blank", " and she said ", "-blank", " is great!"],
    ["I went to the car and saw ", "-blank", ", ugh i ", "-blank","ed out."],
    ["I used GNU/Linux which was made by ", "-blank", " and its ", "-blank"],
    ["GNU GPL is ", "-blank", " and thats great!"],
    ["I copyrighted your ", "-blank", " whatchu gonna do about it ey?"],
    ["-blank", " likes to add ", "-blank", " because ", "-blank"],
    ["I told ", "-blank", " that ", "-blank", " is ", "-blank", "."],
    ["This ", "-blank", " is free ", "-blank", " and is distributed under the ", "-blank", "."]
]


class GamePlayer {
    client: ClientHandler;
    done: boolean = false;
    winAmount: number = 0;

    constructor(client: ClientHandler){ 
        this.client = client;
    }

}

class WriteXWordsScreen extends ServerScreen {
    prefab = "writeXWordsScreen";
    words = new Map<ClientHandler, Array<string>>();

    ready() {
        let game = this.world.game as XiatcGame;

        game.clients.forEach(client => {
            this.words.set(client, new Array<string>());
        });

        this.messageHandler.on("word", (sender, data: PacketSendWord) => {
            if(this.words.get(sender).length >= 8) return;
            
            this.words.get(sender).push(data.word);
            if(this.words.get(sender).length >= 8) {
                this.emitTo(sender, "wordsCompleted", null);   
                this.world.room.updatePlayerList();    
            }

            let done = true;
            this.words.forEach((value) => {
                if(value.length < 8) {
                    done = false;
                }
            });

            if(done) {
                this.completeGame();
            }
        });
    }

    getClientStatus(client: ClientHandler): string {
        return "done: " + (this.words.get(client).length >= 8);
    }

    getCompletionData() {
        return this.words;
    }
}

class CompletePromptScreen extends ServerScreen {
    prefab = "completePromptScreen";
    words: Map<ClientHandler, string[]>;
    assignedPrompt: string[];
    generatedSentences = new Map<ClientHandler, string>();
    doneClients = new Map<ClientHandler, boolean>();

    constructor(words: Map<ClientHandler, Array<string>>){
        super();
        this.words = words;
    }

    ready() {
        this.assignedPrompt = prompts[Math.floor(Math.random()*prompts.length)];    
    
        this.emit("prompt", {
            sentence: this.assignedPrompt
        } as PacketSendSentence);
        
        this.world.game.clients.forEach(client => {
            this.emitTo(client, "words", {
                words: this.words.get(client)
            } as PacketSendWords);
            this.doneClients.set(client, false);
        });
     
        this.messageHandler.on("words", (sender, data: PacketPickWords) => {
            let generatedSentence = "";
            let i = 0;
            data.words.forEach((value) => {
                if(value < 0) {
                    return;
                }
            })
            this.assignedPrompt.forEach(value => {
                if(value == "-blank") {
                    generatedSentence += this.words.get(sender)[data.words[i++]];
                } else {
                    generatedSentence += value;
                }
            });
            this.generatedSentences.set(sender, generatedSentence);
            this.doneClients.set(sender, true);
            
            let gameDone = true;
            this.doneClients.forEach((done, client) => {
                if(!done) {
                    gameDone = false;
                }
            });
            if(gameDone){ 
                this.completeGame();
            }
        });

    }

    getCompletionData() {
        return this.generatedSentences;
    }

    getClientStatus(client: ClientHandler): string {
        return "done: " + (this.doneClients.get(client));
    }

}

class VotingScreen extends ServerScreen {
    prefab = "votingScreen";
    sentencesMap: Map<ClientHandler, string>;
    votes: Array<number>;
    doneClients = new Map<ClientHandler, boolean>();
    sentences = new Array<string>();
    winner: ClientHandler;

    constructor(sentences: Map<ClientHandler, string>) {
        super();
        this.sentencesMap = sentences;
    }

    ready() {
        this.votes = new Array<number>();

        this.world.game.clients.forEach(client => {
            this.doneClients.set(client, false);
        });

        {
            let i = 0;
            this.sentencesMap.forEach((value) => {
                this.sentences.push(value);
                this.votes.push(0);
            });
        }

        this.emit("sentences", {
            sentences: this.sentences
        } as PacketSendSentences);
    
    
        this.messageHandler.on("vote", (sender, data: PacketVoteSentence) => {
            if(this.doneClients.get(sender)) return;
            this.votes[data.sentenceId] += 1;
            
            console.log(this.votes);

            this.emit("vote", {
                votes: this.votes
            } as PacketSendVotes);

            this.doneClients.set(sender, true);

            let gameDone = true;
            this.doneClients.forEach(done => {
                if(!done)
                    gameDone = false;
            });

            if(gameDone) {
                let maxVotes = 0;
                let index = 0;
                this.votes.forEach((value, i) => {
                    if(value > maxVotes) {
                        maxVotes = value;
                        index = i;
                    }
                });

                this.winner = this.world.game.clients[index];

                this.emit("sentenceWon", {
                    sentenceId: index
                } as PacketSentenceWon);

                this.completeGame();
            }
        });
    }

    getCompletionData() {
        return this.winner;
    }

    getClientStatus(client: ClientHandler): string {
        return "done: " + (this.doneClients.get(client));
    }
}

class GameState extends ServerGameObject {
    prefab = "gamestate";
    public players = new Map<ClientHandler, GamePlayer>();
    currentScreen: ServerScreen;

    async ready() {
        this.world.game.clients.forEach((client, i) => {
            this.players.set(client, new GamePlayer(client));
        });

        let player_words: Map<ClientHandler, Array<string>> = await this.runScreen(new WriteXWordsScreen());
        
        let aggregate: Array<string> = []
        player_words.forEach((words, player) => {
            aggregate.push(...words);
        });
        

        for(let i = 0; i < 3; i++) {
            //SHUFFLE
            player_words.forEach((words, player) => {
                let res: Array<string> = [];
                shuffle_array_in_place(aggregate);
                for(let i = 0; i < 8; i++) {
                    res.push(aggregate[i]);
                }
                player_words.set(player, res);
            });

            let sentences = await this.runScreen(new CompletePromptScreen(player_words));
            let winner = await this.runScreen(new VotingScreen(sentences));
            this.players.get(winner).winAmount += 1;
            await this.waitFrames(60);
        }

        let winAmount = 0;
        let winner: ClientHandler;

        this.players.forEach(player => {
            if(winAmount <= player.winAmount) {
                winAmount = player.winAmount;
                winner = player.client;
            }
        });

        this.world.room.alert(winner.name + " won with " + winAmount + " wins.");
        this.world.room.toLobby();
    }
   

    async runScreen(screen: ServerScreen) {
        if(this.currentScreen) {
            this.world.queueRemoveChild(this.currentScreen);
        }
        this.currentScreen = screen;  
        this.world.addChild(screen);  
        return await this.currentScreen.waitUntilCompleted();
    }

    getClientStatus(client: ClientHandler) {
        return this.currentScreen.getClientStatus(client);
    }
}

export class XiatcGame extends GameBase {
    name = "xiatcGame";
    world: NetworkWorld;
    gamestate: GameState;

    async start() {
        this.room.changeScreen("screenGame");
        this.world = new NetworkWorld(this.room, this);
        this.gamestate = new GameState();
        this.world.addChild(this.gamestate);

        this.setInterval(() => this.world.update(), 100);
    }

    getClientStatus(client: ClientHandler) {
        return this.gamestate.getClientStatus(client);
    }
}