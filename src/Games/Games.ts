import { GameBase } from "../GameBase";
import { Room } from "../Room";
import { DrawGuessGame } from "./DrawGuess/DrawGuess";
import { FiascGame } from "./Fiasc/FiascGame";
import { HipsGame } from "./Hips/HipsGame";
import { LiacGame } from "./Liac/LiacGame";
import { SpleefGame } from "./Spleef/SpleefGame";
import { XiatcGame } from "./Xiatc/XiatcGame";

interface GameInstantiator {
    new(room: Room): GameBase;
}

export class GameManager {
    games = new Map<string, GameInstantiator>();

    constructor() {
        this.games.set("hipsGame", HipsGame);
        this.games.set("fiascGame", FiascGame);
        this.games.set("liacGame", LiacGame);
        this.games.set("xiatcGame", XiatcGame);
        this.games.set("drawGuessGame", DrawGuessGame);
        this.games.set("spleefGame", SpleefGame);
    }
}