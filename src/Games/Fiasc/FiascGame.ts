import { GameBase } from "../../GameBase";
import { NetworkWorld } from "../../NetworkWorld";
import { ServerGameObject } from "../../ServerGameObject";
import { PacketSendStories, PacketSendText } from "../../../client/Games/Fiasc/shared/FiascPackets";
import { ClientHandler } from "../../ClientHandler";
import { rotate } from "../../../client/shared/Utils";

class GameState extends ServerGameObject {
    prefab = "gamestate";
    stories = new Array<string>();
    player_story_id = new Array<number>();
    player_done = new Map<ClientHandler, boolean>();
    rounds = 10;

    ready() {
        this.world.game.clients.forEach((client, i) => {
            this.stories.push("");
            this.player_story_id.push(i);
            this.player_done.set(client, false);
        });

        this.messageHandler.on("text", (sender, data: PacketSendText) => {
            if(this.player_done.get(sender) || this.rounds <= 0) return;
            this.player_done.set(sender, true);

            this.stories[this.player_story_id[this.world.game.clientsId.get(sender)]] += " " + data.text;

            let allDone = true;
            this.player_done.forEach(value => {
                if(!value) 
                    allDone = false;
            })

            if(allDone) {
                this.world.game.clients.forEach(client => this.player_done.set(client, false));
                this.player_story_id = rotate(this.player_story_id, 1);
                this.rounds -= 1;

                this.world.game.clients.forEach((client, i) => {
                    let story = this.stories[this.player_story_id[i]];
                    this.emitTo(client, "prompt", {
                        text: story.substring(Math.max(0, story.length-16))
                    })
                });
            }

            if(this.rounds <= 0) {
                this.emit("prompt", {
                    text: ""
                } as PacketSendText);
                this.emit("stories", {
                    stories: this.stories
                } as PacketSendStories);
            }

            this.world.room.updatePlayerList();
        });

        this.messageHandler.on("backToLobby", (sender, data) => {
            this.world.room.toLobby();
        });
    }

}

export class FiascGame extends GameBase {
    name = "fiascGame";
    world: NetworkWorld;
    gamestate: GameState;

    async start() {
        this.room.changeScreen("screenGame");
        this.world = new NetworkWorld(this.room, this);
        this.gamestate = new GameState();
        this.world.addChild(this.gamestate);
    }

    getClientStatus(client: ClientHandler) {
        return "done: " + this.gamestate.player_done.get(client);
    }
}