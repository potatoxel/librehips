//#region PREAMBLE
/*
    This is a multiplayer game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { PacketAlert, PacketPlayerList, PacketSwitchGame, PacketSwitchScene } from '../client/shared/Packets.js';
import { ClientHandler } from './ClientHandler.js';
import { GameBase } from './GameBase.js';
import { FiascGame } from './Games/Fiasc/FiascGame.js';
import { HipsGame } from './Games/Hips/HipsGame.js';
import { Server } from './Server';


export class Room {
    private clients = new Set<ClientHandler>();
    private server: Server;
    public game: GameBase;
    public gameType: string;

    constructor(server: Server) {
        this.server = server;
    }
    
    broadcast(type: string, data: any, except: Set<ClientHandler> = null) {
        this.server.broadcastIn(this.clients, type, data, except);
    }
    
    addClient(client: ClientHandler) {
        this.clients.add(client);
        this.updatePlayerList();
        client.emit("changeScene", {
            newScene: "screenRoomLobby"
        } as PacketSwitchScene);
        
        client.disconnected.connect(() => {
            if(this.clients.has(client)) {
                this.removeClient(client);
            }
        });
    }
    
    removeClient(client: ClientHandler) {
        this.clients.delete(client);
        this.updatePlayerList();
    }
    
    updatePlayerList() {
        let packet: PacketPlayerList = {
            players: []
        };
        
        this.clients.forEach((clientHandler) => {
            let status = "";
            if(this.game) {
                status = ", " + this.game.getClientStatus(clientHandler)
            }
            packet.players.push({
                name: clientHandler.name,
                status: "(ready: " + clientHandler.ready + status + ")"
            });
        });
        
        this.broadcast("playerList", packet);
    }

    getArePlayersReady() {
        let res = true;
        this.clients.forEach((clientHandler) => { 
            if(!clientHandler.ready) {
                res = false;
            }
        });
        return res;
    }

    start() {
        this.startGame(new (this.server.gameManager.games.get(this.gameType))(this));
        this.clients.forEach((client) => {
            client.ready = false;
        });
        this.updatePlayerList();
    }

    changeScreen(scene: string) {
        this.broadcast("changeScene", {
            newScene: scene
        } as PacketSwitchScene);
    }

    startGame(game: GameBase) {
        if(this.game) {
            this.game.dispose();
        }
        this.game = game;
        this.clients.forEach(client => {
            game.addClient(client);
        });
        this.broadcast("changeGame", {
            name: game.name
        } as PacketSwitchGame);
        this.game.start();
    }

    alert(prompt: string) {
        this.broadcast("alert", {
            message: prompt
        } as PacketAlert);
    }

    chooseGame(name: string) {
        this.gameType = name;
        this.broadcast("chooseGame", {
            name: name
        } as PacketSwitchGame);
    }

    toLobby() {
        this.game.dispose();
        this.game = null;
        this.updatePlayerList();
        this.broadcast("disposeGame", null);
        this.changeScreen("screenRoomLobby");
    }
}