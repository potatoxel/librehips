//#region PREAMBLE
/*
    This is a multiplayer game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import express = require('express');
import ws from 'ws';
import {MessageHandler} from '../client/shared/MessageHandler';
import { ReceivedData, PacketCreateJoinRoom, PacketAlert, PacketSwitchScene, PacketPlayerReady, EmitForGameObjectData, PacketSwitchGame} from '../client/shared/Packets';
import { Room } from './Room';
import { Server } from './Server';
import { Signal } from "../client/shared/Signal.js";

export class ClientHandler {
    
    private webSockets: ws;
    private messageHandler: MessageHandler<ClientHandler>;
    private server: Server;
    public name: string = "#DEFAULT";
    public ready: boolean = false;
    public room: Room;
    public disconnected = new Signal<[]>();

    constructor(server: Server, webSockets: ws) {
        this.server = server;
        this.webSockets = webSockets;
        this.messageHandler = new MessageHandler();

        console.log("New client connected!");        
    }

    public setup() {
        console.log("Setup client handler");
    }

    initializeEvents() {
        this.webSockets.on("message", (data) => {
            var json_data: ReceivedData = JSON.parse(data.toString());
            this.messageHandler.handle(this, json_data);
        });

        this.webSockets.on('close', (code, reason) => {
            this.disconnected.emitSignal();
        });

        this.messageHandler.on("createRoom", (sender, data: PacketCreateJoinRoom) => {
            if (this.server.rooms.has(data.roomid)) {
                this.emit("alert", {
                    message: "Room " + data.roomid + " already exists!"
                } as PacketAlert);
            } else if(!this.room) {
                const room = new Room(this.server);
                this.room = room;
                this.name = data.nickname;
                this.server.rooms.set(data.roomid, room);
                this.emit("alert", {
                    message: "Room " + data.roomid + " created!"
                } as PacketAlert);
                room.addClient(this);     
            }
        });

        this.messageHandler.on("joinRoom", (sender, data: PacketCreateJoinRoom) => {
            if(this.server.rooms.has(data.roomid) && !this.room) {
                this.name = data.nickname;
                this.room = this.server.rooms.get(data.roomid);
                this.room.addClient(this);
            }
        });

        this.messageHandler.on("playerReady", (sender, data: PacketPlayerReady) => {
            this.ready = data.ready;
            this.room.updatePlayerList();
            if(this.room.getArePlayersReady()) {
                this.room.start();
            }
        });

        this.messageHandler.on("emitForGameObject", (sender: ClientHandler, data: EmitForGameObjectData) => {
            if(this.room.game)
                this.room.game.world.getChild(data.id).messageHandler.handle(this, data.json);
        });

        this.messageHandler.on("chooseGame", (sender: ClientHandler, data: PacketSwitchGame) => {
            this.room.chooseGame(data.name);
        });

        this.messageHandler.on("ping", (sender: ClientHandler, _data: {}) => {
           //DO Nothing and hope nginx is happy.
        });
    }

    emitString(data: string) {
        this.webSockets.send(data);
    }

    emit(type: string, json: object) {
        this.emitString(JSON.stringify({
            type: type,
            json: json
        } as ReceivedData));
    }
}
