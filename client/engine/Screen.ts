import { ClientGameObject } from "../engine/ClientGameObject.js";

export class ClientScreen extends ClientGameObject {
    protected dom: HTMLDivElement;
    protected completeGame: () => void = () => null;

    constructor() {
        super();
        this.dom = document.createElement("div");
    }

    renderDOM() {
        var dom = this.world.game.dom;
        this.removeDOM();
        dom.appendChild(this.dom);
    }

    removeDOM() {
        var dom = this.world.game.dom;
        if (dom.contains(this.dom)){
            dom.removeChild(this.dom);
        }
    }

    onBeforeDelete() {
        this.removeDOM();
    }

    ready() {
        this.renderDOM();
    }

    getCompletionData(): any {
        return null;
    }

    waitUntilCompleted(): Promise<any> {
        return new Promise<any>((executor, reject) => {
            this.completeGame = () => {
                executor(this.getCompletionData());
            };
        });
    }
}
