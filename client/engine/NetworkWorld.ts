//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { EmitForGameObjectData, LocalPlayerIdData, RemoveGameObjectData, SpawnGameObjectData } from "../shared/Packets.js";
import { Socket } from "../Socket.js";
import { Vector3 } from "../shared/Vector3.js";
import { World } from "../shared/World.js";
import { PrefabInstantiator } from "../shared/GameObjectInstantiator.js";
import { GameObject } from "../shared/GameObject.js";
import { Mouse } from "../Mouse.js";
import { GameBase } from "../Games/GameBase.js";
import { Signal } from "../shared/Signal.js";

export class NetworkWorld extends World {
    public playerId: number;  
    public socket: Socket;
    public instantiator = new PrefabInstantiator();
    public canvas: HTMLCanvasElement;
    public context: CanvasRenderingContext2D;
    public mouse: Mouse;
    public game: GameBase;
    public playerIdRecieved = new Signal<[number]>();

    constructor(socket: Socket, canvas: HTMLCanvasElement, game: GameBase) {
        super();
        this.socket = socket;
        if(canvas){
            this.canvas = canvas;
            this.context = canvas.getContext('2d');
            this.mouse = new Mouse(canvas);
        }
        
        this.game = game;
        
        socket.on("emitForGameObject", (data: EmitForGameObjectData) => {
            this.children.get(data.id).messageHandler.handle(socket, data.json);
        });

        socket.on("spawnGameObject", (data: SpawnGameObjectData) => {
            this.addChild(this.instantiator.instantiate(data));
        });

        socket.on("removeGameObject", (data: RemoveGameObjectData) => {
            this.removeChildById(data.id);
        });

        socket.on("receiveLocalPlayerId", (data: LocalPlayerIdData) => {
            this.playerId = data.id;
            this.playerIdRecieved.emitSignal(data.id);
        });

    }

    findEntitiesWithinRadius(position: Vector3, radius: number) {
        let res: Array<GameObject> = [];
        this.children.forEach((gameObject, key, map) => {
            if(gameObject instanceof GameObject) {
                if(gameObject.position.sub(position).length() <= radius) {
                    res.push(gameObject);
                }
            }
        });
        return res;
    }
}
