//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { Vector3 } from "./Vector3.js";

export class Vector2 {
    public x: number;
    public y: number;
    
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    rotate90(amount: number, size: Vector2) {
        let res = new Vector2(this.x, this.y);
        for(let i = 0; i < amount; i++) {
            [res.x, res.y] = [size.y-res.y,res.x];
        }
        return res;
    }

    
    sub(vector: Vector2) {
        return new Vector2(this.x - vector.x, this.y - vector.y);
    }
    
    add(vector: Vector2) {
        return new Vector2(this.x + vector.x, this.y + vector.y);
    }
    
    pmul(vector: Vector2) {
        return new Vector2(this.x * vector.x, this.y * vector.y);
    }
    
    pdiv(vector: Vector2) {
        return new Vector2(this.x / vector.x, this.y / vector.y);
    }
    
    mul(scalar: number) {
        return new Vector2(this.x * scalar, this.y * scalar);
    }
    
    div(scalar: number) {
        return new Vector2(this.x / scalar, this.y / scalar);
    }
    
    addInPlace(vector: Vector2) {
        this.x += vector.x;
        this.y += vector.y;
    }
    
    subInPlace(vector: Vector2) {
        this.x -= vector.x;
        this.y -= vector.y;
    }
    
    pmulInPlace(vector: Vector2) {
        this.x *= vector.x;
        this.y *= vector.y;
    }
    
    pdivInPlace(vector: Vector2) {
        this.x /= vector.x;
        this.y /= vector.y;
    }
    
    mulInPlace(scalar: number) {
        this.x *= scalar;
        this.y *= scalar;
    }

    divInPlace(scalar: number) {
        this.x /= scalar;
        this.y /= scalar;
    }
    
    clone() {
        return new Vector2(this.x, this.y);
    }

    toVector3(z: number = 0): Vector3 {
        return new Vector3(this.x, this.y, z);
    }

    map(f: (x: number) => number) {
        return new Vector2(f(this.x), f(this.y))
    }

    equalxy(x: number, y: number) {
        return this.x == x && this.y == y;
    }

    sadd(x: number): Vector2 {
        return new Vector2(this.x+x, this.y+x);
    }
}
