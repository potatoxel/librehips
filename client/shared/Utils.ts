export function rotate<T>(array: T[], amount: number): T[] {
    let res = new Array<T>();
    res.length = array.length;
    for(let i = 0; i < array.length; i++) {
        res[mod(i+amount, array.length)] = array[i];
    }
    return res;
}

export function mod(x: number, y: number): number {
    return (x % y + y) % y;
}

export function shuffle_array_in_place<T>(array: T[]) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}