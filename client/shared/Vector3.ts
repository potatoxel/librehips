//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { Vector2 } from "./Vector2.js";

export class Vector3 {
    public x: number;
    public y: number;
    public z: number;

    constructor(x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    rotate90(amount: number, size: Vector3) {
        let res = new Vector3(this.x, this.y, 0);
        let [sx, sy] = [size.x, size.y];
        for(let i = 0; i < amount; i++) {
            [res.x, res.y] = [res.y, sx-res.x-1];
            [sx, sy] = [sy, sx];
        }
        return res;
    }

    sub(vector: Vector3) {
        return new Vector3(this.x - vector.x, this.y - vector.y, this.z - vector.z);
    }
    
    add(vector: Vector3) {
        return new Vector3(this.x + vector.x, this.y + vector.y, this.z + vector.z);
    }

    pmul(vector: Vector3) {
        return new Vector3(this.x * vector.x, this.y * vector.y, this.z * vector.z);
    }
    
    pdiv(vector: Vector3) {
        return new Vector3(this.x / vector.x, this.y / vector.y, this.z / vector.z);
    }
    
    mul(scalar: number) {
        return new Vector3(this.x * scalar, this.y * scalar, this.z * scalar);
    }
    
    div(scalar: number) {
        return new Vector3(this.x / scalar, this.y / scalar, this.z / scalar);
    }
    
    length() {
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }

    equals(vector: Vector3) {
        return vector.x == this.x && vector.y == this.y && vector.z == this.z;
    }

    sum() {
        return this.x + this.y + this.z;
    }

    abs() {
        return new Vector3(Math.abs(this.x), Math.abs(this.y), Math.abs(this.z));
    }

    clone(): Vector3 {
        return new Vector3(this.x, this.y, this.z);
    } 

    swapxy() {
        return new Vector3(this.y, this.x, this.z);
    }

    get4Neighbors() {
        return [
            this.add(new Vector3(1, 0, 0)),
            this.add(new Vector3(-1, 0, 0)),
            this.add(new Vector3(0, 1, 0)),
            this.add(new Vector3(0, -1, 0))
        ]
    }

    toString() {
        return this.x + "," + this.y + "," + this.z;
    }

    toVector2() {
        return new Vector2(this.x, this.y);
    }

    addInPlace(v: Vector3) {
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
    }
}
