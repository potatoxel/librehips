import { Vector2 } from "./Vector2.js";

export class Rect {
    public position = new Vector2(0, 0);
    public size = new Vector2(0, 0);

    constructor(position: Vector2, size: Vector2) {
        this.position = position;
        this.size = size;
    }

    public get topLeft() {
        return this.position.clone();
    }

    public get topRight() {
        return this.position.add(new Vector2(this.size.x, 0));
    }

    public get bottomLeft() {
        return this.position.add(new Vector2(0, this.size.y));
    }

    public get bottomRight() {
        return this.position.add(this.size);
    }
    
    public get center() {
        return this.position.add(this.size.div(2));
    }

    public intersectsWith(rect: Rect) {
        return !(
            this.position.x + this.size.x < rect.position.x 
            || this.position.y + this.size.y < rect.position.y
            || rect.position.x + rect.size.x < this.position.x
            || rect.position.y + rect.size.y < this.position.y);
    }

    public containsPoint(point: Vector2) {
        return this.position.x < point.x && point.x < this.position.x + this.size.x 
            && this.position.y < point.y && point.y < this.position.y + this.size.y;
    }
}