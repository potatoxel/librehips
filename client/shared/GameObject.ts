//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import {MessageHandler} from "./MessageHandler.js"
import { SpawnGameObjectData } from "./Packets.js";
import { Vector3 } from "./Vector3.js";
import { World } from "./World.js";

export class GameObject {
    world: World;

    public messageHandler = new MessageHandler();
    public id: number;
    public clientOwned: boolean = false;
    private everyFunctions: Array<{
        func: () => void,
        period: number,
        timeLeft: number,
        once: boolean
    }> = [];
    public movable = false;
    public collidable = false;

    public position: Vector3 = new Vector3(0, 0, 0);

    update() {
        //TODO: dont make an array every frame?
        let deleteList = new Array<any>();
        this.everyFunctions.forEach((value, index, array) => {
            value.timeLeft -= 1;
            if (value.timeLeft <= 0) {
                value.func();
                value.timeLeft = value.period;
                if(value.once) {
                    deleteList.push(value);
                }
            }
        });
        deleteList.forEach((obj) => {
            this.everyFunctions.splice(this.everyFunctions.indexOf(obj), 1);
        });
    }

    draw() {

    }

    guiDraw() {
        
    }

    public init(data: SpawnGameObjectData) {
        
    }

    public ready() {

    }

    public collidesWith(gameObject: GameObject) {
        return false;
    }

    public collidesWithPoint(position: Vector3) {
        return false;
    }

    public preciseCollidesWithPoint(position: Vector3) {
        return false;
    }

    public every(period: number, func: () => void, immediately: boolean = true, once: boolean = false) {
        this.everyFunctions.push({
            period: period,
            func: func,
            timeLeft: immediately ? 0 : period,
            once: once
        });
    }

    public waitFrames(amount: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.every(amount, resolve, false, true);            
        });
    }

    public onBeforeDelete() {
        
    }
}
