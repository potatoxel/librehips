//#region PREAMBLE
/*
    This is a libre drawing game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { Vector2 } from "./shared/Vector2.js";


export class Mouse {
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    mouseDown: boolean = false;
    position: Vector2 = new Vector2(0, 0);

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;

        this.canvas.addEventListener("mousedown", (ev) => this.onMouseDown(this.getPos(new Vector2(ev.clientX, ev.clientY))));
        this.canvas.addEventListener("mousemove", (ev) => this.onMouseMove(this.getPos(new Vector2(ev.clientX, ev.clientY))));
        this.canvas.addEventListener("mouseup", (ev) => this.onMouseUp(this.getPos(new Vector2(ev.clientX, ev.clientY))));
        this.context = this.canvas.getContext('2d');
    }

    onMouseDown(position: Vector2): any {
        this.mouseDown = true;
    }

    onMouseUp(position: Vector2): any {
        this.mouseDown = false;
    }

    onMouseMove(position: Vector2): any {
        this.position = position;
    }
    
    getPos(position: Vector2) {
        var rect = this.canvas.getBoundingClientRect();
        return new Vector2(
            position.x - rect.left,
            position.y - rect.top
        );
    }
}