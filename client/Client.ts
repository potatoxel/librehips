//#region PREAMBLE
/*
    This is a multiplayer game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion
import { PacketAlert, PacketCreateJoinRoom, PacketPlayerList, PacketPlayerReady, PacketSwitchGame, PacketSwitchScene } from "./shared/Packets.js";
import { Socket } from "./Socket.js";
import { HipsGame } from "./Games/Hips/HipsGame.js";
import { Keyboard } from "./Keyboard.js";
import { Mouse } from "./Mouse.js";
import { GameBase } from "./Games/GameBase.js";
import { GameManager } from "./Games/Games.js";


const socket: Socket = new Socket();
socket.connect((window.location.protocol.endsWith("s:") ? "wss" : "ws") + "://" + window.location.host + "/server");

const screenMain = document.getElementById("screenMain");
let currentScene = screenMain;

export const keyboard = new Keyboard();
keyboard.hookToWindow();

let currentGame: GameBase;
const gameManager = new GameManager();


socket.on("alert", (data: PacketAlert) => {
    alert(data.message);
})

socket.on("changeScene", (data: PacketSwitchScene) => {
    currentScene.classList.add("hidden");    
    currentScene = document.getElementById(data.newScene);
    currentScene.classList.remove("hidden");
})

socket.on("playerList", (data: PacketPlayerList) => {
    let playerListHTML = document.createElement("div");
    console.log(data);
    data.players.forEach((player) => {
        let playerHTML = document.createElement("p");
        playerHTML.innerText = player.name + " " + player.status;
        playerListHTML.appendChild(playerHTML);
    })
    let playerLists = document.getElementsByClassName("playerList");
    for(let i = 0; i < playerLists.length; i++) {
        let playerList = playerLists[i];
        playerList.innerHTML = "";
        playerList.appendChild(playerListHTML.cloneNode(true));
    }
});

socket.on("changeGame", (data: PacketSwitchGame) => {
    if(currentGame) {
        currentGame.dispose();
    }
    currentGame = new (gameManager.games.get(data.name))(socket.getSocketWrapper());
});

socket.on("disposeGame", () => {
    if(currentGame)
        currentGame.dispose();
});

document.getElementById("createRoomButton").onclick = () => {
    const nicknameText = document.getElementById("nickname") as HTMLInputElement;
    const roomIdText = document.getElementById("roomId") as HTMLInputElement;
    socket.emit("createRoom", {
        nickname: nicknameText.value,
        roomid: roomIdText.value
    } as PacketCreateJoinRoom)
}

document.getElementById("joinRoomButton").onclick = () => {
    const nicknameText = document.getElementById("nickname") as HTMLInputElement;
    const roomIdText = document.getElementById("roomId") as HTMLInputElement;
    socket.emit("joinRoom", {
        nickname: nicknameText.value,
        roomid: roomIdText.value
    } as PacketCreateJoinRoom)
}

document.getElementById("startGameButton").onclick = () => {
    socket.emit("playerReady", {
        ready: true
    } as PacketPlayerReady);
}

/////////////////////game selector/////////////////////
const gameList = document.getElementById("gameList");
const gameListButtons = new Map<string, HTMLButtonElement>();

gameManager.games.forEach((value, key) => {
    const p = document.createElement("p");
    const button = document.createElement("button");
    p.appendChild(button);
    gameList.appendChild(p);
    gameListButtons.set(key, button);
    console.log(key);
    
    button.innerText = key;
    button.onclick = () => {
        socket.emit("chooseGame", {
            name: key
        } as PacketSwitchGame);
    }
});

socket.on("chooseGame", (data: PacketSwitchGame) => {
    gameListButtons.forEach(button => {
        button.innerText = button.innerText.replace("> ", "");
    });
    gameListButtons.get(data.name).innerText = "> " + gameListButtons.get(data.name).innerText;
});


setInterval(() => {
    socket.emit("ping", {});
}, 2000);