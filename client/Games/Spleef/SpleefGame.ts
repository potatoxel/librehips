import { keyboard } from "../../Client.js";
import { Clickable } from "../../Components/Clickable.js";
import { ClientGameObject } from "../../engine/ClientGameObject.js";
import { NetworkWorld } from "../../engine/NetworkWorld.js";
import { Mouse } from "../../Mouse.js";
import { GameObject } from "../../shared/GameObject.js";
import { Vector2 } from "../../shared/Vector2.js";
import { Vector3 } from "../../shared/Vector3.js";
import { Socket } from "../../Socket.js";
import { GameBase } from "../GameBase.js";



class Tilemap extends ClientGameObject {
    map: number[][][];
    size = new Vector3(16, 16, 4);
    tilesize = new Vector2(32, 32);

    ready() {
        let num = 0;
        let getNum = () => {
            num += 1;
            return num % 3 == 0 ? 1 : 0;
        }
        this.map = Array(this.size.x).fill([])
                .map(
                    _=> Array(this.size.y).fill([])
                        .map( 
                            _=> Array(this.size.z).fill(0).map(_ => 1) 
                        )
                )
        
        this.messageHandler.on("setBlock", (sender, data: any) => {
            this.map[data.x][data.y][data.z] = data.value
        });
    }

    draw() {
        let spleef = this.world.game as SpleefGame;
        spleef.player.grounded = false;

        for(let z = 0; z < this.size.z; z++) {
            spleef.drawingFunctions.push({
                z: z,
                draw: () => {
                    for(let y = 0; y < this.size.y; y++) {
                        for(let x = 0; x < this.size.x; x++) {
                            let newZ = z+(this.size.z-1-spleef.player.position.z);
                            let shade = 120 + newZ*20;
                            let sizeOffset = this.getSizeOffset(newZ);
                            
                            let offset = this.getRenderOffset(newZ);

                            if (this.map[x][y][z] == 1 && newZ < this.size.z+1) {
                                //temp bad code cuz im tired
                                //let playerTilePos = spleef.player.position.toVector2().sub(offset.toVector2()).pdiv(this.tilesize.sadd(2 + sizeOffset)).map(Math.floor);
                                let playerTilePos = this.getTilePosition(spleef.player.position);

                                if (Math.abs(spleef.player.position.z - z) <= 0.05 && spleef.player.position.z > z) {
                                    if (playerTilePos.equalxy(x,y)) {
                                        spleef.player.grounded = true;
                                        shade = 100;
                                    }

                                    if (spleef.mouse.mouseDown) {
                                        let mouseTilePosition = this.getTilePosition(
                                            new Vector3(spleef.mouse.position.x, spleef.mouse.position.y, spleef.player.position.z).sub(spleef.getCameraTranslation().toVector3())
                                        );
    
                                        if(mouseTilePosition.equalxy(x,y)) {
                                            this.map[x][y][z] = 0;
                                            this.emit("setBlock", {
                                                x, y, z, value: 0
                                            })
                                        }    
                                    }
                                    
                                }


                                let playerBelowZ = spleef.player.position.z < z;
                                this.world.context.fillStyle = "rgba(" + shade + ", " + shade + ", " + shade + "," + (playerBelowZ ? 0.3 : 1) + ")";

                                this.world.context.fillRect(
                                    this.position.x + x * (this.tilesize.x + 2 + sizeOffset) + offset.x,
                                    this.position.y + y * (this.tilesize.y + 2 + sizeOffset) + offset.y,
                                    this.tilesize.x + sizeOffset,
                                    this.tilesize.y + sizeOffset);
                                }
                                    
                        }
                                
                    }
                }
            });
        }
            
    }

    private getSizeOffset(globalZ: number) {
        return globalZ * 30;
    }

    private getRenderOffset(globalZ: number) {
        let spleef = this.world.game as SpleefGame;
        return spleef.player.position.mul((globalZ) / (this.size.z - 1)).mul(-1).add(spleef.player.position);
    }

    getCurrentRenderOffset(z: number) {
        let spleef = this.world.game as SpleefGame;
        return this.getRenderOffset(z+(this.size.z-1-spleef.player.position.z));
    }

    getCurrentSizeOffset(z: number) {
        let spleef = this.world.game as SpleefGame;
        return this.getSizeOffset(z+(this.size.z-1-spleef.player.position.z));
    }

    getTilePosition(globalPosition: Vector3) {
        let z = Math.floor(globalPosition.z);
        return globalPosition.toVector2()
                .sub(this.getCurrentRenderOffset(z).toVector2())
                .pdiv(this.tilesize.sadd(2 + this.getCurrentSizeOffset(z)))
                .map(Math.floor)
    }
}

class Player extends ClientGameObject {
    alive = true;
    speed = 4;
    size = new Vector2(32, 32);
    grounded = false;
    ungroundedFor = 0;
    velocity = new Vector3(0,0,0); //mostly for jump

    update() {
        if(this.id != this.world.playerId) return;

        if(keyboard.isKeyDown("w")) {
            this.position.y -= this.speed;
            this.emitPosition();
        }
        if(keyboard.isKeyDown("a")) {
            this.position.x -= this.speed;
            this.emitPosition();
        }
        if(keyboard.isKeyDown("s")) {
            this.position.y += this.speed;
            this.emitPosition();
        }
        if(keyboard.isKeyDown("d")) {
            this.position.x += this.speed;
            this.emitPosition();
        }
       
        if(!this.grounded) {
            this.ungroundedFor += 1;
            if(this.ungroundedFor > 7){
                this.position.z -= 0.005;
                this.emitPosition();
            }
        } else {
            this.ungroundedFor = 0;

            if (keyboard.isKeyJustDown(" ")) {
                this.velocity = new Vector3(0, 0, 0.1);
            }
        }

        this.position.addInPlace(this.velocity);
        this.velocity = this.velocity.mul(0.99);
    }

    draw() {
        
        let spleef = this.world.game as SpleefGame;
        let player = spleef.player;
        let z = spleef.player.position.z;
        
        let dz = this.position.z - z;
        let size = this.size.sadd(dz*5).map(x => x > 0 ? x : 1);
        let position = this.position.add(spleef.tilemap.getCurrentRenderOffset(this.position.z))
        
        spleef.drawingFunctions.push({
            z: this.position.z,
            draw: () => {
                this.world.context.fillStyle = "#00ff00";
                this.world.context.fillRect(position.x, position.y, size.x, size.y);
            }
        });
      
    }
}

class GameState extends ClientGameObject {

    ready() {
        
    }

}

export class SpleefGame extends GameBase {
    canvas: HTMLCanvasElement;
    socket: Socket;
    networkWorld: NetworkWorld;
    mouse: Mouse;
    player: Player;
    tilemap: Tilemap;
    canvasSize: Vector2;
    drawingFunctions = new Array<{
        z: number,
        draw: () => void
    }>();

    constructor(socket: Socket) {
        super(socket);

        this.canvas = document.createElement("canvas");
        this.canvas.width = 600;
        this.canvas.height = 600;
        this.dom.appendChild(this.canvas);

        this.mouse = new Mouse(this.canvas);
        
        this.socket = socket;
        this.networkWorld = new NetworkWorld(this.socket, this.canvas, this);
        this.networkWorld.instantiator.bind("gamestate", GameState);
        this.networkWorld.instantiator.bind("tilemap", Tilemap);
        this.networkWorld.instantiator.bind("player", Player);
        this.networkWorld.instantiator.onInstance("tilemap", (tilemap) => this.tilemap = tilemap as Tilemap);

        this.networkWorld.playerIdRecieved.connect((id) => {
            this.player = this.networkWorld.getChild(id) as Player;
        });

        this.canvasSize = new Vector2(this.canvas.width, this.canvas.height);
        this.setInterval(() => {
            this.networkWorld.update();
            this.networkWorld.context.resetTransform();
            
            this.networkWorld.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            
            let pos = this.getCameraTranslation();
            this.networkWorld.context.translate(pos.x, pos.y);

            this.drawingFunctions = [];
            this.networkWorld.draw();
            this.drawingFunctions.sort((a, b) => a.z - b.z);
            this.drawingFunctions.forEach((draw => {
                if (draw.z < this.player.position.z+1) {
                    draw.draw();
                }
            }));

            keyboard.update();
        }, 1000/60);
    }

    public getCameraTranslation() {
        return this.player.size.div(-2).sub(this.player.position.toVector2()).add(this.canvasSize.div(2));
    }
}