import { Socket } from "../Socket.js";

export class GameBase {
    socket: Socket;
    dom: HTMLDivElement;
    private timers = new Set<NodeJS.Timeout>();

    constructor(socket: Socket) {
        this.socket = socket;
        this.dom = document.getElementById("game") as HTMLDivElement;
        this.dom.innerHTML = "";
    }

    dispose() {
        this.socket.disconnectAll();
        this.timers.forEach((value) => {
            clearInterval(value);
        });
    }

    setInterval(func: () => void, time: number) {
        this.timers.add(setInterval(() => {
            func()
        }, time));
    }

}