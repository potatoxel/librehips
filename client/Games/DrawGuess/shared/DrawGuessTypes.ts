export interface PacketAlert {
    message: string;
}

export interface PacketPlayerReady {
    ready: boolean;
}

export interface PacketSendPrompt {
    prompt: string;
}

export interface PacketSendImage {
    image: string;
}

export interface PacketOptions {
    options: Array<{
        name: string;
        suboptions: Array<string>;
    }>;
}

export interface PacketShowImage {
    prompt: string;
    id: number;
}

export interface PacketStoreImage {
    original_prompt: string;
    info: Drawing;
}

export interface PacketSetImageToView {
    prompt: string,
    view: number
}

export type Drawing = {
    prompt: string;
    drawing: string;
    guess: string;
};