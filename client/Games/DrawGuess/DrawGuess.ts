import { Clickable } from "../../Components/Clickable.js";
import { ClientGameObject } from "../../engine/ClientGameObject.js";
import { NetworkWorld } from "../../engine/NetworkWorld.js";
import { ClientScreen } from "../../engine/Screen.js";
import { Mouse } from "../../Mouse.js";
import { Vector2 } from "../../shared/Vector2.js";
import { Socket } from "../../Socket.js";
import { GameBase } from "../GameBase.js";
import { DrawingCanvas } from "./DrawingCanvas.js";
import { Drawing, PacketOptions, PacketSendImage, PacketSendPrompt, PacketSetImageToView, PacketShowImage, PacketStoreImage } from "./shared/DrawGuessTypes.js";


class PromptScreen extends ClientScreen {
    prefab = "promptScreen";

    ready() {
        super.ready();
        let game = this.world.game as DrawGuessGame;
        game.canvas.style.display = "none";
        game.label.textContent = "Write a prompt c:";
        
        let { form, input } = makeInput();
        this.dom.appendChild(form);

        form.onsubmit = (ev) => {
            this.emit("prompt", {
                prompt: input.value
            } as PacketSendPrompt)
            ev.preventDefault();
        }
    }
}

class DrawingScreen extends ClientScreen {
    prefab = "drawingScreen";

    ready() {
        super.ready();
        let game = this.world.game as DrawGuessGame;
        game.canvas.style.display = "block";
        game.drawingCanvas.clear();

        game.label.textContent = "Draw a: ~~LOADING~~";

        this.messageHandler.on("prompt", (sender, data: PacketSendPrompt)=>{
            game.label.textContent = "Draw a: " + data.prompt;
        });

        let submit = document.createElement("button");
        this.dom.appendChild(submit);

        submit.textContent = "done";
        submit.onclick = () => {
            submit.disabled = true;
            this.emit("submit", {
                image: game.drawingCanvas.getImageURL()
            })
        }
    }
}

class GuessingScreen extends ClientScreen {
    prefab = "guessingScreen";

    ready() {
        super.ready();
        let game = this.world.game as DrawGuessGame;
        game.canvas.style.display = "block";
        
        game.label.textContent = "Guess this imagE!";

        let { form, input } = makeInput();
        this.dom.appendChild(form);

        form.onsubmit = (ev) => {
            this.emit("submit", {
                prompt: input.value
            } as PacketSendPrompt)
            ev.preventDefault();
        }

        this.messageHandler.on("image", (sender, data: PacketSendImage) => {
            game.drawingCanvas.drawImageFromURL(data.image);
        });
    }
}


class ViewingScreen extends ClientScreen { 
    prefab = "viewingScreen";
    label: HTMLLabelElement;
    offline: boolean = true;

    ready() {
        super.ready()
        let game = this.world.game as DrawGuessGame;
        game.canvas.style.display = "block";

        this.label = game.label;
        game.label.textContent = "VIEWING THING! TWO :3";

        let options: {name: string, suboptions: string[]}[] = new Array(); 

        game.gamestate.images.forEach((value, key) => {
            let arr: string[] = [];
            for(let i = 0; i < value.length; i++) {
                arr.push(i.toString());
            }

            options.push({ 
                name: key,
                suboptions: arr
            })
        });

        //Could be moved back into the options message handler if need other options from server
        options.forEach(option => {
            let p = document.createElement("p");
            p.innerText = option.name;
            option.suboptions.forEach((somethingToView) => {
                let button = document.createElement("button");
                button.innerText = somethingToView;
                button.onclick = () => {
                    this.emit("setImageToView", {
                        prompt: option.name,
                        view: Number.parseInt(somethingToView)
                    } as PacketSetImageToView);
                    this.showImageByPromptAndId(option.name, Number.parseInt(somethingToView));
                }
                p.appendChild(button);
            });
            this.dom.appendChild(p);
        });

        this.messageHandler.on("options", (sender, data: PacketOptions) => {
            let backToLobby = document.createElement("button");
            backToLobby.textContent = "BACK TO LOBBY";
            this.dom.appendChild(backToLobby);
    
            backToLobby.onclick = (ev) => {
                this.emit("backToLobby", {});
            }
        });

        this.messageHandler.on("showImage", (sender, data: PacketShowImage) => {
            this.showImageByPromptAndId(data.prompt, data.id);
        });

       
    }

    showImageByPromptAndId(prompt: string, id: number) {
        let game = this.world.game as DrawGuessGame;
        game.canvas.style.display = "block";

        var drawing = game.gamestate.images.get(prompt)[id];
        game.drawingCanvas.drawImageFromURL(drawing.drawing);
        this.label.textContent = "Prompt: " + drawing.prompt + ", Guess: " + drawing.guess;
    }
}


class GameState extends ClientGameObject {
    images = new Map<string, Array<Drawing>>();
    
    ready() {
        this.messageHandler.on("storeImage", (sender, data: PacketStoreImage) => {
            var image_list: Array<Drawing>;
            if(this.images.has(data.original_prompt)) {
                image_list = this.images.get(data.original_prompt);
            } else {
                image_list = [];
                this.images.set(data.original_prompt, image_list);
            }
            image_list.push(data.info);
        })
    }


   

}

export class DrawGuessGame extends GameBase {
    canvas: HTMLCanvasElement;
    label: HTMLLabelElement;
    drawingCanvas: DrawingCanvas;
    socket: Socket;
    networkWorld: NetworkWorld;
    mouse: Mouse;
    gamestate: GameState;

    constructor(socket: Socket) {
        super(socket);
        
        this.label = document.createElement("label");
        this.dom.appendChild(this.label);

        this.canvas = document.createElement("canvas");
        this.canvas.width = 600;
        this.canvas.height = 600;
        this.dom.appendChild(this.canvas);

        let viewImages = document.createElement("button");
        viewImages.textContent = "locally view images (PRESS IT ONLY IF GAME BORKED)";
        viewImages.style.marginTop = "32px";
        this.dom.appendChild(viewImages);

        viewImages.onclick = () => {
            this.networkWorld.addChild(new ViewingScreen());
        };

        this.drawingCanvas = new DrawingCanvas(this.canvas);

        this.mouse = new Mouse(this.canvas);
        
        this.socket = socket;
        this.networkWorld = new NetworkWorld(this.socket, this.canvas, this);
        this.networkWorld.instantiator.bind("gamestate", GameState);
        this.networkWorld.instantiator.bind("promptScreen", PromptScreen);
        this.networkWorld.instantiator.bind("drawingScreen", DrawingScreen);
        this.networkWorld.instantiator.bind("guessingScreen", GuessingScreen);
        this.networkWorld.instantiator.bind("viewingScreen", ViewingScreen);

        this.networkWorld.instantiator.onInstance("gamestate", (gamestate: GameState) => {
            this.gamestate = gamestate;
        })
    }


}

function makeInput() {
    let form = document.createElement("form");
    let label = document.createElement("label");
    label.textContent = "Prompt: ";
    let input = document.createElement("input");

    form.appendChild(label);
    form.appendChild(input);
    return { form, input };
}


