//#region PREAMBLE
/*
    This is a libre drawing game.
    Copyright (C) 2021 potatoxel <potatoxel.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

export class DrawingCanvas {
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    mouseDown: boolean = false;
    prevPosition: {x: number, y: number};

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;

        this.canvas.addEventListener("mousedown", (ev) => this.startDrawing(this.getPos({x: ev.clientX, y: ev.clientY})));
        this.canvas.addEventListener("mousemove", (ev) => this.draw(this.getPos({x: ev.clientX, y: ev.clientY})));
        this.canvas.addEventListener("mouseup", (ev) => this.stopDrawing(this.getPos({x: ev.clientX, y: ev.clientY})));
        this.context = this.canvas.getContext('2d');
    }

    startDrawing(position: { x: number; y: number; }): any {
        this.prevPosition = position;
        this.mouseDown = true;
        this.context.beginPath();
        this.context.ellipse(position.x, position.y, 8, 8, 0, 0, Math.PI*2);
        this.context.fill();
    }

    stopDrawing(position: { x: number; y: number; }): any {
        this.mouseDown = false;
    }

    draw(position: { x: number; y: number; }): any {
        if(this.mouseDown) {
            this.context.fillStyle = "#f00ff0";
            this.context.strokeStyle = "#f00ff0";
            this.context.lineWidth = 16;
            this.context.lineCap = "round";
            this.context.beginPath();
            this.context.moveTo(this.prevPosition.x, this.prevPosition.y);
            this.context.lineTo(position.x, position.y);
            this.context.stroke();
            this.prevPosition = position;
        }
    }
    
    getPos(position: {x: number, y: number}) {
        var rect = this.canvas.getBoundingClientRect();
        return {
            x: position.x - rect.left,
            y: position.y - rect.top
        }
    }

    clear() {
        this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    getImageURL() {
        return this.canvas.toDataURL();
    }

    drawImageFromURL(url: string) {
        const context = this.canvas.getContext("2d");
        const img = document.createElement("img");
        img.src = url;
        img.onload = () => {
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            context.drawImage(img, 0, 0);
        }
    }
}