export interface PacketSendWord {
    word: string;
}

export interface PacketSendWords {
    words: Array<string>;
}


export interface PacketSendSentence {
    sentence: Array<string>; //"-blank" means fill in here
}

export interface PacketSendVotes {
    votes: Array<number>;
}

export interface PacketPickWords {
    words: Array<number>;
}

export interface PacketSendSentences {
    sentences: Array<string>;
}

export interface PacketSentenceWon {
    sentenceId: number;
}

export interface PacketVoteSentence {
    sentenceId: number;
}

export interface PacketShowWinner {
    votes: Array<number>;
    winner: number;
}