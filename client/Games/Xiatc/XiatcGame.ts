import { ClientGameObject } from "../../engine/ClientGameObject.js";
import { NetworkWorld } from "../../engine/NetworkWorld.js";
import { Socket } from "../../Socket.js";
import { GameBase } from "../GameBase.js";
import { ClientScreen } from "../../engine/Screen.js";
import { PacketPickWords, PacketSendSentence, PacketSendSentences, PacketSendVotes, PacketSendWord, PacketSendWords, PacketSentenceWon, PacketVoteSentence } from "./shared/XiatcPackets.js";

class WriteXWordsScreen extends ClientScreen {
    form: HTMLFormElement;
    label: HTMLLabelElement;
    input: HTMLInputElement;
    prefab = "writeXWordsScreen";

    ready() {
        super.ready();

        this.form = document.createElement("form");

        this.label = document.createElement("label");
        this.label.innerText = "Write 8 words: ";
        this.form.appendChild(this.label);
        
        this.input = document.createElement("input");
        this.form.appendChild(this.input);

        this.dom.appendChild(this.form);

        this.form.onsubmit = ev => {
            ev.preventDefault();
            this.emit("word", {
                word: this.input.value
            } as PacketSendWord)
            this.input.value = "";
        };

        this.messageHandler.on("wordsCompleted", (sender, data) => {
            this.input.disabled = true;
        });
    }

}

class CompletePromptScreen extends ClientScreen {
    ready() {
        super.ready();
        
        let prompt = document.createElement("div");
        let choices = document.createElement("div");
        let footer = document.createElement("div");
        let slotId = 0;
        this.dom.appendChild(prompt);
        this.dom.appendChild(choices);
        this.dom.appendChild(footer);

        let done = document.createElement("button");
        done.innerText = "Done!";
        footer.appendChild(done);
        
        let promptInputs = new Array<HTMLInputElement>();
        let words = new Array<number>();

        done.onclick = ev => {
            this.emit("words", {
                words: words
            } as PacketPickWords);
        };

        this.messageHandler.on("prompt", (sender, data: PacketSendSentence) =>{
            data.sentence.forEach(part => {
                if(part == "-blank") {
                    let input = document.createElement("input");
                    input.disabled = true;
                    input.style.backgroundColor = "#ffffff";
                    input.style.color = "#000000";
                    promptInputs.push(input);
                    words.push(-1);
                    prompt.appendChild(input);
                } else {
                    let span = document.createElement("span");
                    span.innerText = part;
                    prompt.appendChild(span);
                }
            });
        });

        this.messageHandler.on("words", (sender, data: PacketSendWords) => {
            data.words.forEach((value, i) => {
                let choice = document.createElement("button");
                choice.innerText = value;
                choices.appendChild(choice);
                
                choice.onclick = ev => {
                    promptInputs[slotId].value = value;
                    words[slotId] = i;
                    slotId++;
                    slotId %= promptInputs.length;
                }
            });
            
        });
    }
}

class VotingScreen extends ClientScreen {
    ready() {
        super.ready();
        
        let options = document.createElement("div");
        this.dom.appendChild(options);

        let votes = new Array<HTMLLabelElement>();

        this.messageHandler.on("sentences", (sender, data: PacketSendSentences) => {
            data.sentences.forEach((value, i) => {
                let p = document.createElement("p");
                let choice = document.createElement("button");
                choice.innerText = value;
                p.appendChild(choice);

                let vote = document.createElement("label");
                vote.innerText = " (0)";
                p.appendChild(vote);

                votes.push(vote);

                options.appendChild(p);

                choice.onclick = ev => {
                    this.emit("vote", {
                        sentenceId: i
                    } as PacketVoteSentence);
                };
            });
        });

        this.messageHandler.on("vote", (sender, data: PacketSendVotes)=> {
            data.votes.forEach((value,i)=>{
                votes[i].innerText = " (" + value + ")";
            });
        });

        this.messageHandler.on("sentenceWon", (sender, data: PacketSentenceWon) => {
            votes[data.sentenceId].innerText = " >>>> " + votes[data.sentenceId].innerText;
        });
    }
}


class GameState extends ClientGameObject {
    
}

export class XiatcGame extends GameBase {
    world: NetworkWorld;

    constructor(socket: Socket) {
        super(socket);
        this.world = new NetworkWorld(socket, null, this);
        this.world.instantiator.bind("gamestate", GameState);
        this.world.instantiator.bind("writeXWordsScreen", WriteXWordsScreen);
        this.world.instantiator.bind("completePromptScreen", CompletePromptScreen);
        this.world.instantiator.bind("votingScreen", VotingScreen);
    }


}