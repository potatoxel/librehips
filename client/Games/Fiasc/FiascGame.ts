import { ClientGameObject } from "../../engine/ClientGameObject.js";
import { NetworkWorld } from "../../engine/NetworkWorld.js";
import { Socket } from "../../Socket.js";
import { GameBase } from "../GameBase.js";
import { PacketSendText, PacketSendStories } from "./shared/FiascPackets.js";

class GameState extends ClientGameObject {
    form: HTMLFormElement;
    prompt: HTMLLabelElement;
    text: HTMLInputElement;

    ready() {
        this.form = document.createElement("form");
        this.prompt = document.createElement("label");
        this.text = document.createElement("input");
        this.form.appendChild(this.prompt);
        this.form.appendChild(this.text);

        let submitBtn = document.createElement("input");
        submitBtn.type = "submit";
        submitBtn.style.display = "none";
        this.form.appendChild(submitBtn);
  
        this.world.game.dom.appendChild(this.form);

        this.form.addEventListener("submit", ev => {
            ev.preventDefault();
            this.emit("text", {
                text: this.text.value
            } as PacketSendText);
            this.text.value = "";
        });

        this.messageHandler.on("prompt", (sender, data: PacketSendText) => {
            this.prompt.innerText = data.text;
        });
 
        this.messageHandler.on("stories", (sender, data: PacketSendStories) => {
            this.form.remove();
            const dom = this.world.game.dom;
            dom.innerHTML = "";
            data.stories.forEach((value) => {
                let p = document.createElement("p");
                p.innerText = value;
                dom.appendChild(p);
            });

            const button = document.createElement("button");
            button.innerText = "Back to lobby!";
            dom.appendChild(button);
            
            button.onclick = () => {
                this.emit("backToLobby", null);
            }
        });
    }

}

export class FiascGame extends GameBase {
    world: NetworkWorld;

    constructor(socket: Socket) {
        super(socket);
        this.world = new NetworkWorld(socket, null, this);
        this.world.instantiator.bind("gamestate", GameState);
    }

}