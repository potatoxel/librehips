export interface PacketSendText { 
    text: string;
}

export interface PacketSendStories {
    stories: Array<string>;
}