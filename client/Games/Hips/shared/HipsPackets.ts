export interface WinnerInfoPacket {
    id: number,
    isPlayer: boolean,
    name: string
}
