import { keyboard } from "../../Client.js";
import { ClientGameObject } from "../../engine/ClientGameObject.js";
import { NetworkWorld } from "../../engine/NetworkWorld.js";
import { Mouse } from "../../Mouse.js";
import { Socket } from "../../Socket.js";
import { GameBase } from "../GameBase.js";
import { WinnerInfoPacket } from "./shared/HipsPackets.js";

class Living extends ClientGameObject {
    alive = true;

    ready() {
        this.messageHandler.on("died", (sender, data) => {
            this.alive = false;
        });
    }
}

class Player extends Living {
    alive = true;

    update() {
        if(this.id != this.world.playerId) return;
        if(!this.alive) return;

        if(keyboard.isKeyDown("w")) {
            this.position.x += 1;
            this.emitPosition();
        } else if(keyboard.isKeyDown("e")) {
            this.position.x += 5;
            this.emitPosition();
        }
    }

    draw() {
        this.world.context.fillStyle = this.alive ? "#ff0000" : "#ffffff";
        this.world.context.fillRect(this.position.x, this.position.y, 32, 32);
    }
}

class NPC extends Living {
    draw() {
        this.world.context.fillStyle = this.alive ? "#ff0000" : "#ffffff";
        this.world.context.fillRect(this.position.x, this.position.y, 32, 32);
    }
}


class Cursor extends ClientGameObject { 
    bulletUsed = false;

    ready() {
        this.messageHandler.on("shoot", () => {
            this.bulletUsed = true;
        });
    }

    update() {
        if(!this.owner) return;
        this.position = this.world.mouse.position.toVector3();
        this.emitPosition();

        if(this.world.mouse.mouseDown && !this.bulletUsed) {
            this.bulletUsed = true;
            this.emit("shoot", null);
            let entities = this.world.findEntitiesWithinRadius(this.position, 32);
            for(let i=0; i<entities.length; i++) {
                let player = entities[i];
                if(player instanceof Living) {
                    (player as Living).emit("shoot", null);
                    break;
                }
            }
        }
    }

    draw() {
        this.world.context.strokeStyle = this.bulletUsed ? "#ffffff" : "#ff0000";
        this.world.context.strokeRect(this.position.x-32, this.position.y-32, 64, 64);
        this.world.context.strokeRect(this.position.x-4,this.position.y-4,8,8);
    }
}

class GameState extends ClientGameObject {

    ready() {
        this.messageHandler.on("win", (sender, data: WinnerInfoPacket) => {
            alert("The winner is " + data.name);
            //TODO: do something better than an alert
        });
    }

}

export class HipsGame extends GameBase {
    canvas: HTMLCanvasElement;
    socket: Socket;
    networkWorld: NetworkWorld;
    mouse: Mouse;

    constructor(socket: Socket) {
        super(socket);

        this.canvas = document.createElement("canvas");
        this.canvas.width = 600;
        this.canvas.height = 600;
        this.dom.appendChild(this.canvas);
        
        this.socket = socket;
        this.networkWorld = new NetworkWorld(this.socket, this.canvas, this);
        this.networkWorld.instantiator.bind("player", Player);
        this.networkWorld.instantiator.bind("cursor", Cursor);
        this.networkWorld.instantiator.bind("npc", NPC);
        this.networkWorld.instantiator.bind("gamestate", GameState);

        this.setInterval(() => {
            this.networkWorld.update();
            
            this.networkWorld.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.networkWorld.draw();
        }, 100);
    }


}