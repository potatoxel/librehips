import { Clickable } from "../../Components/Clickable.js";
import { ClientGameObject } from "../../engine/ClientGameObject.js";
import { NetworkWorld } from "../../engine/NetworkWorld.js";
import { Mouse } from "../../Mouse.js";
import { Vector2 } from "../../shared/Vector2.js";
import { Socket } from "../../Socket.js";
import { GameBase } from "../GameBase.js";



class Point extends ClientGameObject {
    clickable: Clickable;

    ready() {
        let game = this.world.game as LiacGame;
        this.clickable = new Clickable(game.mouse);
        this.clickable.onClick.connect(() => {
            this.emit("click", null);
        });
    }
    
    update() {
        this.clickable.rect.position = this.position.toVector2();
        this.clickable.rect.size = new Vector2(32, 32);
        this.clickable.update();
    }

    draw() {
        this.world.context.fillStyle = "#ff0000";
        this.world.context.fillRect(this.position.x, this.position.y, 32, 32);
    }

}

class GameState extends ClientGameObject {

    ready() {
        
    }

}

export class LiacGame extends GameBase {
    canvas: HTMLCanvasElement;
    socket: Socket;
    networkWorld: NetworkWorld;
    mouse: Mouse;

    constructor(socket: Socket) {
        super(socket);

        this.canvas = document.createElement("canvas");
        this.canvas.width = 600;
        this.canvas.height = 600;
        this.dom.appendChild(this.canvas);

        this.mouse = new Mouse(this.canvas);
        
        this.socket = socket;
        this.networkWorld = new NetworkWorld(this.socket, this.canvas, this);
        this.networkWorld.instantiator.bind("point", Point);
        this.networkWorld.instantiator.bind("gamestate", GameState);

        this.setInterval(() => {
            this.networkWorld.update();
            
            this.networkWorld.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.networkWorld.draw();
        }, 100);
    }


}