import { Socket } from "../Socket.js";
import { DrawGuessGame } from "./DrawGuess/DrawGuess.js";
import { FiascGame } from "./Fiasc/FiascGame.js";
import { GameBase } from "./GameBase.js"
import { HipsGame } from "./Hips/HipsGame.js";
import { LiacGame } from "./Liac/LiacGame.js";
import { SpleefGame } from "./Spleef/SpleefGame.js";
import { XiatcGame } from "./Xiatc/XiatcGame.js";

interface GameInstantiator {
    new(socket: Socket): GameBase;
}

export class GameManager {
    games = new Map<string, GameInstantiator>();

    constructor() {
        this.games.set("hipsGame", HipsGame);
        this.games.set("fiascGame", FiascGame);
        this.games.set("liacGame", LiacGame);
        this.games.set("xiatcGame", XiatcGame);
        this.games.set("drawGuessGame", DrawGuessGame);
        this.games.set("spleefGame", SpleefGame);
    }
}