import { Mouse } from "../Mouse.js";
import { Rect } from "../shared/Rect.js";
import { Signal } from "../shared/Signal.js";
import { Vector2 } from "../shared/Vector2.js";

export class Clickable {
    rect = new Rect(new Vector2(0,0), new Vector2(0,0));
    lastMouseDown: boolean = false;
    mouse: Mouse;
    onClick = new Signal<[]>();

    constructor(mouse: Mouse) {
        this.mouse = mouse;
    }

    update() {
        if(this.mouse.mouseDown && !this.lastMouseDown && this.rect.containsPoint(this.mouse.position)) {
            this.onClick.emitSignal();
        }
        this.lastMouseDown = this.mouse.mouseDown;
    }
}